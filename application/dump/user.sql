CREATE TABLE  `go_swing`.`user` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`oath_id` INT( 255 ) NOT NULL ,
`fullname` VARCHAR( 255 ) NOT NULL
) ENGINE = MYISAM ;

SELECT u.id, u.oath_id, u.fullname, c.type, c.value
FROM user AS u
  INNER JOIN (

               SELECT ci.id, ci.user_id, ct.value as type, ci.value
               FROM contact AS ci
                 INNER JOIN contact_type AS ct
               WHERE ci.type = ct.id
             ) AS c
WHERE c.user_id = u.id
      AND u.id =1
