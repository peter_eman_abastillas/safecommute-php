<?php
/**
 * Created by IntelliJ IDEA.
 * User: jatazoulja
 * Date: 6/26/15
 * Time: 11:02 AM
 */

class User extends MOO_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Users');
        $this->model = $this->Users;
        if($this->input->server('REQUEST_METHOD') != "POST") {
            $this->auth = $this->seg();
        }
    }

    public function get()
    {
        $auth = parent::get();
        $data = array(
            "message" => "user exist",
            "data" => $auth
        );
        $this->load->view("return", $data);
    }


    public function post() {
        $data = json_decode(file_get_contents("php://input", "r"));
        $required = array("oath", "fullname", "mdn", "imei");
        foreach($required as $re) {
            if(!array_key_exists($re, $data)) {
                $this->load->view('REST', array("code" => 400));
                die();
            }
        }
        $post = $this->_post($data);
        $data = array(
            "message" => "success",
            "data" => $post
        );
        $this->load->view("return", $data);

    }

    /**
     * @param null $data
     * @Overide;
     */
    public function _post($data) {
        $isExisting = $this->model->checkIfExist($data->oath);
        if($isExisting->num_rows() > 0) {
            return $isExisting->result_array();
        }

        return $this->model->postUser($data);
    }

    public function loadError() {
        $this->load->view('REST', array("code" => 401));
    }
}