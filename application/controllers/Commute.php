<?php
/**
 * Created by IntelliJ IDEA.
 * User: PETEREMAN.ABASTILLAS
 * Date: 6/30/15
 * Time: 11:05 AM
 */
/**
 * Class Commute
 */
class Commute extends MOO_Controller {
    const MAX_TIME_LIMIT = 4;

    public function __construct() {
        parent::__construct();
        $this->load->model('Users');
        $this->load->model('Taxis');
        $this->load->model('Commutes');
        $this->auth = $this->seg();

    }
    /**
     * API index.php/commute/put/id/25/auth/rtywu32t-y43g89-p5opip9-i3j4io309
     * method: put;
     * Payload:
     * {
     *      plate: "string",
     *      time: 0000000000,
     *      lat: 000000.000000,
     *      lang: 000000.000000,
     *      eta: 00000000000
     * }
     * return:
     *  {
     *      status: "success",
     *      message: "success",
     *      payload: {
     *          trans_id: 435,
     *          others: [
     *              // reports goes here...
     *          ]
     *      }
     *  }
     */
    public function put() {
        $data = json_decode(file_get_contents("php://input", "r"));
        $taxiDetails = $this->_getTaxiExist($data->plate);
        // Add Transaction;
        $payload = array(
            "user_id" => $this->auth["id"],
            "taxi_id" => $taxiDetails['id'],
            "lat" => $data->lat,
            "lng" => $data->lng,
            "eta" => $data->eta
        );
        // Check Taxi records;

        $ratings = $this->Commutes->CheckCommuteRatings($taxiDetails["id"]);
        $commute = $this->Commutes->addCommuteStart($payload);

        $json = array(
            "message" => "success",
            "data" => array(
                "trans_id" => $commute,
                "others" => $ratings
            )
        );
        $this->load->view("return", $json);
    }

    /*
     * API index.php/commute/polling/id/25/auth/rtywu32t-y43g89-p5opip9-i3j4io309
     * date
     * method: put;
     * payload:
     * {
     *      trans_id: 456,
     *      lat: 000000.000000,
     *      lang: 000000.000000,
     *      timelog:  00000000000
     * }
     * return:
     *  null
     */

    public function polling() {
        $data = json_decode(file_get_contents("php://input", "r"));
        $lastTransaction = $this->Commutes->logingJourney($data);
        $tTime = 0;
        $time = $data->timelog;
        $count = 0;
         do {
             $tTime = $this->Commutes->getLastInsertedId($data->trasn_id);
             sleep(60);
             $count++;
        } while($tTime == $time && $count < MAX_TIME_LIMIT );
        die();
    }

    private function _getTaxiExist($plate) {
        $data = $this->Taxis->checkExistingPlate($plate);
        return $data[0];
    }
}