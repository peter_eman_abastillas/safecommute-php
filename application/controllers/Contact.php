<?php
/**
 * Created by IntelliJ IDEA.
 * User: PETEREMAN.ABASTILLAS
 * Date: 6/29/15
 * Time: 10:30 AM
 */

class Contact extends MOO_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Contacts');
        $this->model = $this->Contacts;

        // AUTH is required here!

        $this->auth = $this->seg();

    }
    public function get_type() {
        if(!$this->auth) {
            return $this->_get($this->auth["id"]);
        }
        $data = array(
            "message" => "success",
            "data" => $this->model->getTypes()
        );
        $this->load->view("return", $data);
    }

    public function put() {
        $data = json_decode(file_get_contents("php://input", "r"));
        $required = array(
            "type",
            "name",
            "value"
        );
        $newData = array();
        for($i=0;$i<sizeof($data); $i++) {
            foreach($required as $re) {
                if(!array_key_exists($re, $data[$i])) {
                    var_dump("require $re");
                    $this->load->view('REST', array("code" => 400));
                    die();
                }
            }
            $data[$i]->user_id = $this->auth["id"];
            $newData[] = $this->_put($data[$i]);
        }
        $this->load->view("return",  array(
                "message" => "success",
                "data" => $newData
            ));
    }
    public function _put($data) {
        return $this->model->addContacts($data);
    }

} 