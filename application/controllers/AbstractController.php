<?php
/**
 * Created by IntelliJ IDEA.
 * User: jatazoulja
 * Date: 6/26/15
 * Time: 10:34 AM
 */

interface InterfaceController {
    public function get($id = null);
    public function post($id = null);
    public function put($id = null);
    public function del($id = null);
}

class RestException extends Exception {

}