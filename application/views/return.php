<?php
/**
 * Created by IntelliJ IDEA.
 * User: jatazoulja
 * Date: 6/27/15
 * Time: 10:41 PM
 */

$array = array(
    "status" => "success",
    "message" => $message,
    "payload" => $data
);

echo json_encode($array);