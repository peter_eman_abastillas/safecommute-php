<?php
/**
 * Created by IntelliJ IDEA.
 * User: PETEREMAN.ABASTILLAS
 * Date: 7/8/15
 * Time: 9:50 AM
 */

class Commutes extends CI_Model {
    public $table = "commute";
    public $table_commute_log = "commute_log";
    public $table_rating = "commute_rating";

    public function addCommuteStart($data) {
        $this->db->insert(
            $this->table,
            $data
        );
        return $this->db->insert_id();
    }

    public function CheckCommuteRatings($taxi_id) {
        $query = $this->db->select("*")->from($this->table_rating)->where("taxi_id", $taxi_id)->get();
        return $query->result_array();
    }

    public function logingJourney($data) {
        $this->db->insert(
            $this->table_commute_log,
            $data
        );
        return $this->db->insert_id();
    }

    public function getLastInsertedId($id) {
        $query = $this->db->select("*")->from($this->table_commute_log)->where("commute_id", $id)->get();
        $data = $query->result_array();
        return $data[sizeof($data)-1];
    }

} 