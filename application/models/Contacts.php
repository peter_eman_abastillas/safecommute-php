<?php
/**
 * Created by IntelliJ IDEA.
 * User: PETEREMAN.ABASTILLAS
 * Date: 6/29/15
 * Time: 11:50 AM
 */

class Contacts extends CI_Model {
    public $table = "contact";
    public $contactType = "contact_type";
    public $joinContactType = "";

    public function __construct() {
        parent::__construct();
        $this->joinContactType = "SELECT ci.id, ci.user_id, ct.value as type, ci.value
                                   FROM contact AS ci
                                     INNER JOIN contact_type AS ct
                                   WHERE ci.type = ct.id";
    }

    public function getTypes() {
        $query =  $this->db->get($this->contactType);
        return $query->result();
    }

    public function addContacts($data) {
        $this->db->insert(
            $this->table,
            array(
                "type" => $data->type,
                "name" => $data->name,
                "value" => $data->value,
                "user_id"  => $data->user_id
            )
        );

        return $this->db->insert_id();
    }

} 