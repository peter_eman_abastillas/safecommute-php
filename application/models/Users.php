<?php
/**
 * Created by IntelliJ IDEA.
 * User: jatazoulja
 * Date: 6/26/15
 * Time: 11:33 AM
 */

class Users extends CI_Model {
    public $table = "user";
    private $getUserWithContact;

    public function  __construct() {
        parent::__construct();
        $this->getUserWithContact = "SELECT u.id, c.id as c_id,  u.fullname, u.oath_id,  c.type, c.value, c.name
                                        FROM user AS u
                                          INNER JOIN (
                                                       SELECT ci.id, ci.user_id, ct.value as type, ci.value, ci.name
                                                       FROM contact AS ci
                                                         INNER JOIN contact_type AS ct
                                                       WHERE ci.type = ct.id
                                                     ) AS c
                                        WHERE c.user_id = u.id
                                              AND u.id = ";

    }

    public function get($id = null) {

    }

    public function getByUserId($id) {

        $query = $this->db->query($this->getUserWithContact . $id);
        $result = $query->num_rows();
        switch ($result) {
            case 0:
                $data = null;
                // TODO: Insert!!!
                break;
            default:
                $data = $this->processGetResult($query);
                break;

        }
        return $data;

    }
    public function processGetResult($result) {
        $data = $result->result_array();
        $data2 = array(
            "id" => $data[0]["id"],
            "oath_id" => $data[0]["oath_id"],
            "fullname" => $data[0]["oath_id"],
            "list" => array()
        );

        for($i=0;$i<sizeof($data); $i++) {
            $data[$i]["id"] = $data[$i]["c_id"];
            unset($data[$i]["oath_id"]);
            unset($data[$i]["fullname"]);
            unset($data[$i]["c_id"]);
            $data2["list"][] = $data[$i];
        }
        return $data2;
    }
    public function checkIfExist($id) {
        $query = $this->db->get_where($this->table, array('oath_id' => $id));
        return $query;
    }


    public function postUser($data) {
         $this->db->insert(
            $this->table,
            array(
                "oath_id" => $data->oath,
                "fullname" => $data->fullname,
                "imei" => $data->imei,
                "mdn" => $data->mdn
            )
        );

        return $this->db->insert_id();
    }

    public function getUser($auth, $date) {
        $query = $this->db->select("*")->from($this->table)->where("id", intval ($auth["id"]))->get();
        $compare = $query->result_array();
        $hash = md5($compare[0]["imei"] . $date);
        // var_dump($hash);

        return ($hash == $auth["auth"]);
    }
}