<?php
/**
 * Created by IntelliJ IDEA.
 * User: jatazoulja
 * Date: 6/26/15
 * Time: 6:17 PM
 */
require_once("InterfaceController.php");

abstract class MOO_Controller extends CI_Controller implements InterfaceController {
    public $model;
    public $auth;
    public $headers;

    public function __construct() {
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
        header('Content-Type: application/json');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->headers = apache_request_headers();

    }

    public function get()
    {
        if($this->auth) {
            return $this->_get($this->auth["id"]);
        }
        return false;
    }

    public function post()
    {
        // TODO: Implement post() method.
    }

    public function put()
    {
        // Verify
        // TODO: Implement put() method.
    }

    public function del()
    {
        // TODO: Implement del() method.
    }

    public function _get($id = null)
    {
        $user = $this->model->getByUserId($id);

        return (is_null($user)) ? false : $user;
    }
    public function _post($id = null)
    {
        // TODO: Implement _post() method.
    }

    public function _put($data)
    {

    }

    public function _del($id = null)
    {
        // TODO: Implement _del() method.
    }


    public function seg() {
        $default = $this->uri->ruri_to_assoc();
        if(!$this->testAuth($default)) {
            //throw new RestException();
            header("HTTP/1.0 401 Unauthorized entry");
            $data = array(
                "code" => 401,
                "message" => "Unauthorized entry"
            );
            echo json_encode($data);
            die();
        }
        return $default;
    }


    private function testAuth($auth) {
        if(!isset($this->headers["Date"])) {
            return false;
        }
        $data = $this->Users->getUser($auth, $this->headers["Date"]);
        if(!array_key_exists("auth", $auth)) {
            // echo "test";
            return false;
        }
        return $data;
    }

}