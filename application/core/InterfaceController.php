<?php
/**
 * Created by IntelliJ IDEA.
 * User: jatazoulja
 * Date: 6/26/15
 * Time: 6:17 PM
 */

interface InterfaceController {
    public function get();
    public function post();
    public function put();
    public function _get($id = null);
    public function _post($data);
    public function _put($data);
}